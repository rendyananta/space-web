<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home.index');
});

Route::get('/faq', function () {
    return view('home.faq');
});

Route::get('/cek-ruang', 'EventController@index')->name('event.index');

Route::group(['prefix' => 'auth', 'namespace' => 'Auth', 'as' => 'auth.'], function () {
    Route::group(['middleware' => 'guest'], function () {
        // Login
        Route::get('login', 'LoginController@showLoginForm')->name('login.form');
        Route::post('login', 'LoginController@login')->name('login');

        // Register
        Route::get('register', 'RegisterController@showRegistrationForm')->name('register.form');
        Route::post('register', 'RegisterController@register')->name('register');

        // Forgot password
        Route::get('forgot-password', 'ForgotPasswordController@showLinkRequestForm')->name('password.forgot.form');
        Route::post('forgot-password', 'ForgotPasswordController@sendResetLinkEmail')->name('password.forgot');

        // Reset Password
        Route::get('reset-password/{token}', 'ResetPasswordController@showResetForm')->name('password.reset.form');
        Route::post('reset-password', 'ResetPasswordController@reset')->name('password.reset');
    });

    Route::group(['middleware' => 'auth'], function () {
        Route::get('logout', function () {
            auth()->logout();
            return redirect()->to('/');
        })->name('logout');
    });
});

Route::group(['middleware' => 'auth', 'prefix' => 'aplikasi', 'as' => 'application.'], function () {
    Route::get('/', 'ApplicationController@index')->name('index');
    Route::get('{application}', 'ApplicationController@show')->name('show');
});

Route::group(['prefix' => 'profil', 'middleware' => 'auth', 'as' => 'profile.'], function () {
    Route::get('/', 'ProfileController@index')->name('index');
    Route::get('edit', 'ProfileController@edit')->name('edit');
    Route::put('edit', 'ProfileController@update')->name('update');
    Route::get('/password/reset', 'ProfileController@resetPasswordForm')->name('password.reset.form');
    Route::post('/password/reset', 'ProfileController@resetPassword')->name('password.reset');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/bekerja', 'WorkingController@create')->name('working.create');
    Route::post('/bekerja', 'WorkingController@store')->name('working.store');
    Route::get('/menjadi-relawan', 'VolunteeringController@create')->name('volunteering.create');
    Route::post('/menjadi-relawan', 'VolunteeringController@store')->name('volunteering.store');
    Route::get('/gawe-event', 'EventController@create')->name('event.create');
    Route::post('/gawe-event', 'EventController@store')->name('event.store');
});
