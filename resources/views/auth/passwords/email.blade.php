@extends('layouts.default')

@section('sub-body')
    <h2 class="display mt-5">Lupa Sandi?</h2>

    <div class="row mt-5">
        <div class="col-md-6">
            @include('layouts.components.errors')

            <form action="{{ route('auth.password.forgot') }}" method="post">
                @csrf
                @method("post")
                <div class="form-group">
                    <label for="input-email" class="col-form-label">Surel / email</label>
                    <input type="text" name="email" class="form-control {{ set_error('email') }}" id="input-email">
                    {!! get_error('email') !!}
                </div>
                <div class="form-group">
                    Sudah punya akun ? <a href="{{ route('auth.login.form') }}">Masuk</a>
                </div>
                <div class="form-group">
                    <button class="btn mt-4 btn--hero btn-primary shadow-sm">Kirim</button>
                </div>
            </form>
        </div>
    </div>
@endsection
