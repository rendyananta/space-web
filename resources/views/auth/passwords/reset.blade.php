@extends('layouts.default')

@section('sub-body')
    <h2 class="display mt-5">Atur Ulang Sandi</h2>

    <div class="row mt-5">
        <div class="col-md-6">
            @include('layouts.components.errors')

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form action="{{ route('auth.password.reset') }}" method="post">
                @csrf
                @method("post")

                <input type="hidden" name="token" value="{{ $token }}">
                <input type="hidden" name="email" value="{{ $email }}">

                <div class="form-group">
                    <label for="input-password" class="col-form-label">Sandi Baru</label>
                    <input type="password" name="password" class="form-control {{ set_error('password') }}" id="input-password">
                    {!! get_error('password') !!}
                </div>
                <div class="form-group">
                    <label for="input-password-confirmation" class="col-form-label">Konfirmasi Sandri Baru</label>
                    <input type="password" name="password_confirmation" class="form-control {{ set_error('password_confirmation') }}" id="input-password-confirmation">
                    {!! get_error('password_confirmation') !!}
                </div>
                <div class="form-group">
                    Sudah punya akun ? <a href="{{ route('auth.login.form') }}">Masuk</a>
                </div>
                <div class="form-group">
                    <button class="btn mt-4 btn--hero btn-primary shadow-sm">Masuk</button>
                </div>
            </form>
        </div>
    </div>
@endsection
