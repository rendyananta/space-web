@extends('layouts.default')

@section('sub-body')
    <h2 class="display mt-5">Mendaftar</h2>

    <div class="row mt-5">
        <div class="col-md-6">
            @include('layouts.components.errors')

            <form action="{{ route('auth.register') }}" method="post">
                @csrf
                @method("post")
                <div class="form-group">
                    <label for="input-name" class="col-form-label">Nama</label>
                    <input type="text" name="name" class="form-control {{ set_error('name') }}" id="input-name" value="{{ old('name') }}">
                    {!! get_error('name') !!}
                </div>
                <div class="form-group">
                    <label for="input-email" class="col-form-label">Surel / email</label>
                    <input type="text" name="email" class="form-control {{ set_error('email') }}" id="input-email" value="{{ old('email') }}">
                    {!! get_error('email') !!}
                </div>
                <div class="form-group">
                    <label for="input-dob" class="col-form-label">Tanggal Lahir</label>
                    <input type="date" name="dob" class="form-control {{ set_error('dob') }}" id="input-dob" value="{{ old('dob') }}">
                    {!! get_error('dob') !!}
                </div>
                <div class="form-group">
                    <label for="input-gender" class="col-form-label">Jenis Kelamin</label>
                    <select name="gender" id="input-gender" class="form-control {{ set_error('gender') }}">
                        <option value="">Pilih Jenis Kelamin</option>
                        @foreach(\App\Entities\User::GENDERS as $key => $gender)
                            <option value="{{ $key }}" {{ set_selected(old('gender') == $key) }}>{{ $gender }}</option>
                        @endforeach
                    </select>
                    {!! get_error('gender') !!}
                </div>
                <div class="form-group">
                    <label for="input-occupation" class="col-form-label">Pekerjaan</label>
                    <select name="occupation" id="input-occupation" class="form-control {{ set_error('occupation') }}">
                        <option value="">Pilih Pekerjaan</option>
                        @foreach(\App\Entities\User::OCCUPATIONS as $occupation)
                            <option value="{{ $occupation }}" {{ set_selected(old('occupation') == $occupation) }}>{{ $occupation }}</option>
                        @endforeach
                    </select>
                    {!! get_error('occupation') !!}
                </div>
                <div class="form-group">
                    <label for="input-institution" class="col-form-label">Institusi</label>
                    <input type="text" name="institution" class="form-control {{ set_error('institution') }}" id="input-institution" value="{{ old('institution') }}">
                    {!! get_error('institution') !!}
                </div>
                <div class="form-group">
                    <label for="input-password" class="col-form-label">Sandi</label>
                    <input type="password" name="password" class="form-control {{ set_error('password') }}" id="input-password">
                    {!! get_error('password') !!}
                </div>
                <div class="form-group">
                    <label for="input-password-confirmation" class="col-form-label">Konfirmasi Sandi</label>
                    <input type="password" name="password_confirmation" class="form-control {{ set_error('password_confirmation') }}" id="input-password-confirmation">
                    {!! get_error('password_confirmation') !!}
                </div>
                <div class="form-group">
                    Sudah punya akun ? <a href="{{ route('auth.login.form') }}">Masuk</a>
                </div>
                <div class="form-group">
                    <button class="btn mt-4 btn--hero btn-primary" type="submit">Daftar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
