@extends('layouts.default')

@section('sub-body')
    <h2 class="display mt-5">Selamat Datang</h2>

    <div class="row mt-5">
        <div class="col-md-6">
            @include('layouts.components.errors')

            <form action="{{ route('auth.login') }}" method="post">
                @csrf
                @method("post")
                <div class="form-group">
                    <label for="input-email" class="col-form-label">Surel / email</label>
                    <input type="text" name="email" class="form-control {{ set_error('email') }}" id="input-email">
                    {!! get_error('email') !!}
                </div>
                <div class="form-group">
                    <label for="input-password" class="col-form-label">Sandi</label>
                    <input type="password" name="password" class="form-control {{ set_error('password') }}" id="input-password">
                    {!! get_error('password') !!}
                </div>
                <div class="form-group">
                    Lupa Sandi ? <a href="{{ route('auth.password.forgot.form') }}">Atur ulang</a>
                    <br>
                    Belum menjadi anggota ? <a href="{{ route('auth.register.form') }}">Daftar di sini</a>
                </div>
                <div class="form-group">
                    <button class="btn mt-4 btn--hero btn-primary shadow-sm">Masuk</button>
                </div>
            </form>
        </div>
    </div>
@endsection
