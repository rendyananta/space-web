@extends('layouts.default')

@section('sub-body')
    <section id="main" class="py-5" style="height: 100vh">
        <div class="jumbotron jumbotron-fluid bg-white mt-5">
            <div class="row">
                <div class="col-lg-12 mt-5">
                    <br>
                    <h1 class="display py-5">Kolaborasi Bersama, <br> Membangun Surabaya</h1>
                </div>
            </div>
            <div class="row" style="padding-top: 5%">
                <div class="col-lg-12">
                </div>
                <div class="col-xl-4 col-lg-4 text-center mt-3">
                    <a class="btn btn--hero btn-primary shadow-sm" style="width: 80%" href="{{ route('volunteering.create') }}">Jadi Relawan</a>
                </div>
                <div class="col-xl-4 col-lg-4 text-center mt-3">
                    <a class="btn btn--hero btn-primary shadow-sm" style="width: 80%" href="{{ route('working.create') }}">Bekerja</a>
                </div>
                <div class="col-xl-4 col-lg-4 text-center mt-3">
                    <a class="btn btn--hero btn-primary shadow-sm" style="width: 80%" href="{{ route('event.create') }}">Gawe Event</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>
    <hr>
    <section id="facilities" class="py-5">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="display text-center">Fasilitas Umum</h2>
            </div>
            <div class="col-md-6 col-xs-12 mb-3 mt-5">
                <img src="{{ asset('img/fasilitas.jpg') }}" class="img-fluid" alt="Responsive image">
            </div>
            <div class="col px-5">
                <p class="mt-5">
                    Akses gedung dan internet 24 jam <br>
                    <br>

                    Coworking space untuk umum <br>
                    (luas: 177 m2, kapasitas: 70 orang) <br>
                    <br>

                    Ruang event format teater <br>
                    (luas: 128,8m2, kapasitas: 100 orang) <br>
                    <br>

                    Ruang event format classroom <br>
                    (luas 128,8m2, kapasitas: 60 orang) <br>
                    <br>

                    <br>
                    Setiap ruangan dilengkapi dengan backdrop, sound system, screen, dan projector.
                </p>
            </div>
        </div>
    </section>

    <hr>

    <section id="activities" class="py-5">
        <h2 class="text-center display">Aktivitas</h2>
        <div class="row pt-5">
            <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                <img src="{{ asset('img/aktivitas1.jpg') }}" class="img-fluid img-thumbnail" alt="">
                <br>
                <h5 class="display py-3">Nama Aktivitas</h5>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                <img src="{{ asset('img/aktivitas2.jpg') }}" class="img-fluid img-thumbnail" alt="">
                <br>
                <h5 class="display py-3">Nama Aktivitas</h5>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                <img src="{{ asset('img/aktivitas3.jpg') }}" class="img-fluid img-thumbnail" alt="">
                <br>
                <h5 class="display py-3">Nama Aktivitas</h5>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                <img src="{{ asset('img/aktivitas4.jpg') }}" class="img-fluid img-thumbnail" alt="">
                <br>
                <h5 class="display py-3">Nama Aktivitas</h5>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                <img src="{{ asset('img/aktivitas5.jpg') }}" class="img-fluid img-thumbnail" alt="">
                <br>
                <h5 class="display py-3">Nama Aktivitas</h5>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                <img src="{{ asset('img/aktivitas6.jpg') }}" class="img-fluid img-thumbnail" alt="">
                <br>
                <h5 class="display py-3">Nama Aktivitas</h5>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                <img src="{{ asset('img/aktivitas7.jpg') }}" class="img-fluid img-thumbnail" alt="">
                <br>
                <h5 class="display py-3">Nama Aktivitas</h5>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6 col-xs-12">
                <img src="{{ asset('img/aktivitas8.jpg') }}" class="img-fluid img-thumbnail" alt="">
                <br>
                <h5 class="display py-3">Nama Aktivitas</h5>
            </div>
        </div>
    </section>

    <hr>

    <section id="galleries" class="py-5">
        <h2 class="text-center display">Galeri</h2>
        <div class="row pt-5">
            <div class="col-lg-8">
                <img src="{{ asset('img/galeri3.jpg') }}" class="img-fluid img-thumbnail my-2" alt="">
            </div>
            <div class="col-lg-4">
                <img src="{{ asset('img/galeri2.jpg') }}" class="img-fluid img-thumbnail my-2" alt="">
                <br>
                <img src="{{ asset('img/galeri1.jpg') }}" class="img-fluid img-thumbnail my-2" alt="">
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-12">
                <p class="text-center mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti eius est, eum labore molestias perferendis tenetur. Accusamus, adipisci aliquam animi aperiam assumenda atque aut cupiditate debitis distinctio doloribus eius eligendi eveniet explicabo illo ipsa iure laboriosam laudantium libero magni minima minus natus necessitatibus odit officiis omnis pariatur quam quas, rem reprehenderit sapiente soluta velit vitae, voluptates! Assumenda labore veritatis voluptatem.</p>
            </div>
        </div>
    </section>

    <hr>

    <section id="location" class="py-5">
        <h2 class="text-center display">Lokasi</h2>
        <div class="col-12 text-center mt-5">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.864025425925!2d112.73543671450334!3d-7.25631237330039!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7f942ccdd7ce3%3A0x12072f0108ba519a!2sKORIDOR%20Coworking%20Space!5e0!3m2!1sen!2sid!4v1588612061561!5m2!1sen!2sid" frameborder="0" style="border:0; width: 100%; min-height: 420px" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
        <div class="col-12">
            <p class="text-center mt-4">Jl. Raya Pandugo No. 15, Rungkut, Surabaya. 60297.</p>
        </div>
    </section>

    <hr>

    <section id="partners" class="py-5">
        <h2 class="text-center display">Mitra</h2>
        <div class="row">
            <div class="col-lg-3 col-sm-6 py-3">
                <img src="{{ asset('img/mitra1.jpg') }}" class="img-fluid img-thumbnail my-4" alt="">
            </div>
            <div class="col-lg-3 col-sm-6 py-3">
                <img src="{{ asset('img/mitra2.jpg') }}" class="img-fluid img-thumbnail my-4" alt="">
            </div>
        </div>
    </section>
@endsection
