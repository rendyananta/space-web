@extends('layouts.default')

@section('sub-body')
    <h2 class="display mt-5">Pertanyaan yang sering diajukan</h2>
    <div class="row">
        <div class="col-8">
            <h5 class="mt-5">Apa itu KORIDOR?</h5>
            <div class="panel">
                <br>
                <p>KORIDOR merupakan ruang kerja bersama yang bertujuan mewadahi inovasi dan kolaborasi anak muda kreatif di Surabaya untuk melahirkan karya di bidang kreatif dan teknologi yang membawa manfaat bagi masyarakat dan kota Surabaya.</b></a></p>
            </div>

            <h5 class="mt-5">Fasilitas apa saja yang tersedia di KORIDOR?</h5>
            <div class="panel">
                <br>
                <p>Kamu bisa cek di menu “Fasilitas” pada web KORIDOR atau  <a target="" href="/#facilities"><b>klik di sini </b></a> untuk mengetahui fasilitas apa saja yang bisa kamu dan startup atau komunitasmu gunakan. </b></a></p>
            </div>

            <h5 class="mt-5">Pada jam berapa KORIDOR beroperasi?</h5>
            <div class="panel">
                <br>
                <p>KORIDOR tersedia 24 jam selama 7 hari untuk memfasilitasi seluruh aktivitasmu, seperti bekerja dan menyelenggarakan event setelah mendapat persetujuan dari pihak KORIDOR. </p>
            </div>

            <h5 class="mt-5">Adakah biaya yang dikeluarkan untuk menggunakan fasilitas di KORIDOR?</h5>
            <div class="panel">
                <br>
                <p>KORIDOR tidak memungut biaya alias gratis, bagi kamu yang menjadi bagian dari KORIDOR. Tentunya, kamu harus bertanggungjawab atas segala fasilitas yang digunakan, karena semua yang ada di KORIDOR merupakan milik bersama dan untuk bersama. </p>
            </div>

            <h5 class="mt-5">Bagaimana cara saya mengakses KORIDOR?</h5>
            <div class="panel">
                <br>
                <p>Pertama, kamu harus terlebih dahulu mengakses link registrasi <a target="" href="#"><b>form ini. </b></a> sebelum berkunjung ke KORIDOR. Saat tiba di KORIDOR,  Titipkan kartu identitasmu <b>(KTP/SIM/KITAS atau Kartu Pelajar/Mahasiswa)</b> di meja resepsionis untuk ditukarkan dengan tag KORIDOR. Hal ini dilakukan agar kami bisa kenal lebih dekat denganmu. Jangan lupa untuk selalu pakai tag-mu selama beraktivitas di area KORIDOR.</p>
            </div>

            <br><br>
            <h2 class="text-center">Kontribusi </h2>

            <h5 class="mt-5">Apa yang bisa saya lakukan di KORIDOR?</h5>
            <div class="panel">
                <br>
                <b>a. Bekerja</b>
                <p>Jika kamu memiliki startup atau proyek kreatif yang bertujuan untuk memberi dampak positif kepada masyarakat dan kota Surabaya, isi data diri dan ajukan proposal startup atau projectmu di  <a target="" href="#"><b>form ini. </b></a> Kami akan menghubungimu secepatnya setelah semua syarat pendaftaran sudah kamu isi dengan benar pada link di atas.</p>


                <br>
                <b>b. Menyelenggarakan Event</b>
                <p>Kamu, startupmu, atau komunitasmu sangat bisa untuk mengadakan event di KORIDOR. Asalkan, event-mu sejalan dengan visi Wali Kota Surabaya, Ibu Tri Rismaharini yaitu untuk menjadikan Surabaya pusat kreatif dan teknologi global yang terus-menerus melahirkan inovasi.
                    Ajukan rencana event yang ingin diadakan dengan mengakses www.koridor.space, klik menu “Event” lalu klik pilihan “Gawe Event” atau isi  <a target="" href="#"><b>form ini. </b></a>, kemudian lengkapi isian form dan jangan lupa upload proposal kegiatanmu. Kami akan menyeleksi apakah event kamu memenuhi persyaratan. Jika event kamu terpilih, kami akan segera menghubungimu.</p>

                <br>
                <b>c. Jadi Peserta Event</b>
                <p>Berbagai event dalam bidang teknologi dan kreativitas telah terselenggara di KORIDOR. Untuk mencari tahu apa saja event yang akan datang di KORIDOR, silakan cek akun Instagram KORIDOR di @koridor.space atau akses www.koridor.space, klik menu “Event”, lalu klik pilihan “Upcoming Event”. Jika kamu ingin menjadi peserta dari salah satu event tersebut, lakukan registrasi sesuai petunjuk dan ketentuan dari masing-masing event yang ada di website KORIDOR.</p>

                <br>
                <b>d. Menjadi Resident</b>
                <p>Jika kamu memiliki ide atau inovasi dan bertekad untuk mewujudkannya, jangan ragu untuk berkarya di KORIDOR sebagai resident. Untuk menjadi resident, terlebih dahulu kamu harus lolos dalam seluruh tahapan program 1000 Startup Digital.
                    <br><br>
                    Apa itu program 1000 Startup Digital? Cari tahu selengkapnya pada link berikut <a href="https://1000startupdigital.id/"> https://1000startupdigital.id/. </a></p>

            </div>

            <h5 class="mt-5">Bagaimana jika saya belum bisa berkontribusi melalui ide produk, startup, atau karya nyata? Apakah ada cara lain agar saya tetap bisa berkontribusi?</h5>
            <div class="panel">
                <br>
                <p>Jika kamu ingin menjadi bagian dari pahlawan masa kini dengan menjadikan Kota Surabaya sebagai sentra kreatif dan teknologi di tingkat global bersama kami, ayo gabung sebagai relawan KORIDOR!
                    <br><br>
                    Caranya, kamu cukup mengakses <a href="/">  website ini </a> , klik menu “Relawan”, lalu klik pilihan “Jadi Relawan”, kemudian isi form disertai dengan upload CV/Portfolio kamu. Selanjutnya, kami akan segera menghubungimu.</p>
            </div>

            <br><br>
            <h2 class="text-center">Event</h2>

            <h5 class="mt-5">Apakah semua event yang diajukan di KORIDOR pasti mendapat persetujuan?</h5>
            <div class="panel">
                <br>
                <p>Tidak semua event yang diajukan di KORIDOR pasti disetujui. Setiap proposal wajib melewati proses seleksi oleh tim internal Pemerintah Kota Surabaya. Hanya event yang sesuai dengan visi KORIDOR, tidak bertabrakan dengan agenda yang sudah ada, dan memenuhi standar minimal kualitas event, yang akan mendapat approval dari tim.</p>
            </div>

            <h5 class="mt-5">Mengapa setiap pengajuan event harus diseleksi?</h5>
            <div class="panel">
                <br>
                <p>KORIDOR dibangun sebagai sebuah coworking space dan event space dengan standar program yang tinggi. Maka dari itu, setiap pengajuan rencana event akan melalui tahap seleksi demi menjaga kualitas kegiatan yang berlangsung di KORIDOR.KORIDOR hadir untuk mewadahi berbagai kegiatan terkait pengembangan kapasitas anak muda di Surabaya di bidang kreatif, teknologi, dan kewirausahaan, yang memberikan dampak nyata kepada peserta khususnya dan masyarakat pada umumnya. Hal ini dilakukan agar KORIDOR tidak menjadi tempat pertemuan/rapat, tempat nongkrong, dan tempat pelaksanaan berbagai kegiatan lainnya yang tidak sesuai dengan tujuan awal dari hadirnya KORIDOR.</p>
            </div>

            <h5 class="mt-5">Berapa lama idealnya proposal pengajuan event diajukan sebelum hari H?</h5>
            <div class="panel">
                <br>
                <p>Karena semua rencana event yang diajukan harus melalui proses seleksi oleh tim Pemerintah Kota Surabaya, maka proposal pengajuan rencana event yang ingin dilaksanakan di KORIDOR sebaiknya diajukan maksimal 2 minggu (H-14) sebelum event dilaksanakan.</p>
            </div>

            <h5 class="mt-5">Apakah saya boleh menjadikan alamat KORIDOR sebagai alamat kantor saya?</h5>
            <div class="panel">
                <br>
                <p>Karena KORIDOR secara legal dimiliki dan dikelola oleh Pemerintah Kota Surabaya, maka kamu tidak diperkenankan untuk menggunakan alamat KORIDOR sebagai alamat kantormu. Segala bentuk pemakaian alamat KORIDOR harus seizin Wali Kota Surabaya. Bila di kemudian hari terjadi permasalahan terkait pemakaian alamat KORIDOR, maka pengguna alamat yang akan sepenuhnya bertanggung jawab, serta tidak menutup kemungkinan akan adanya langkah-langkah hukum jika diperlukan.</p>
            </div>

            <br><br>
            <h2 class="text-center">Menghubungi Koridor</h2>

            <h5 class="mt-5">Adakah nomor telepon yang bisa dihubungi?</h5>
            <div class="panel">
                <br>
                <p>Untuk sementara, bisa menghubungi Bagian Hubungan Masyarakat Pemkot Surabaya di 031-5312144 pesawat 603 dan 594</p>
            </div>
        </div>
    </div>
@endsection
