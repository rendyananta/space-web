@extends('layouts.default')

@section('sub-body')
    <h2 class="display mt-5">Daftar Aplikasi</h2>

    <div class="row mt-5">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Kode</th>
                        <th>Jenis</th>
                        <th>Tanggal Dibuat</th>
                        <th>Tanggal Validasi</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @each('application.item_application', $applications, 'application', 'application.item_empty')
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
