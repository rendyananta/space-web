<tr>
    <td class="font-weight-bold">#{{ $application->code }}</td>
    <td>{{ $application->applicable->getApplicationType() }}</td>
    <td>{{ $application->created_at->format('d M Y - H:i') }}</td>
    <td>
        @if($application->status === null)
            -
        @else
            @if($application->status == \App\Entities\Application::STATUS_APPROVED)
                {{ $application->approved_at->format('d M Y - H:i') }}
            @else
                {{ $application->rejected_at->format('d M Y - H:i') }}
            @endif
        @endif
    </td>
    <td>
        {{ $application->status ?? "-" }}
    </td>
    <td>
        <a href="{{ route('application.show', ['application' => $application]) }}">
            >
        </a>
    </td>
</tr>
