@extends('layouts.default')

@section('sub-body')
    <h2 class="display mt-5">{{ $application->applicable->getApplicationType() }} #{{ $application->code }}</h2>

    <div class="row mt-5">
        @if ($application->applicable instanceof \App\Entities\Event)
            @include('application.event.show')
        @elseif ($application->applicable instanceof \App\Entities\WorkingSpace)
            @include('application.working.show')
        @elseif ($application->applicable instanceof \App\Entities\Volunteer)
            @include('application.volunteering.show')
        @endif
    </div>
@endsection
