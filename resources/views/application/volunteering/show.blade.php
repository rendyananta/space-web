<div class="col-md-6">

    <div class="row">
        <div class="col-sm-6">
            Alamat Domisili
        </div>
        <div class="col-sm-6">
            <strong>{{ $application->applicable->address }}</strong>
        </div>

        <div class="col-sm-6">
            No Telepon
        </div>
        <div class="col-sm-6">
            <strong>{{ $application->applicable->phone }}</strong>
        </div>

        <div class="col-sm-6">
            CV
        </div>
        <div class="col-sm-6">
            <strong><a href="{{ asset(Storage::url($application->applicable->cv)) }}">Lihat CV</a></strong>
        </div>

        <div class="col-sm-6">
            Status
        </div>
        <div class="col-sm-6">
            <strong>{{ $application->status ?? "-" }}</strong>
        </div>

        <div class="col-sm-6">
            Disetujui Pada
        </div>
        <div class="col-sm-6">
            <strong>
                @if($application->status === null)
                    -
                @else
                    @if($application->status == \App\Entities\Application::STATUS_APPROVED)
                        {{ $application->approved_at->format('d M Y - H:i') }}
                    @else
                        {{ $application->rejected_at->format('d M Y - H:i') }}
                    @endif
                @endif
            </strong>
        </div>

    </div>

</div>
