@extends('layouts.default')

@section('sub-body')
    <h2 class="display mt-5">Bergabung menjadi relawan</h2>

    <div class="row mt-5">
        <div class="col-md-6">
            @include('layouts.components.errors')

            <form action="{{ route('volunteering.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method("post")
                <div class="form-group">
                    <label for="input-address" class="col-form-label">Alamat Domisili</label>
                    <input type="text" name="address" class="form-control {{ set_error('address') }}" id="input-address" value="{{ old('address') }}">
                    {!! get_error('address') !!}
                </div>
                <div class="form-group">
                    <label for="input-phone" class="col-form-label">No. Telepon</label>
                    <input type="text" name="phone" class="form-control {{ set_error('phone') }}" id="input-phone" value="{{ old('phone') }}">
                    {!! get_error('phone') !!}
                </div>
                <div class="form-group">
                    <label for="input-cv" class="col-form-label">Unggah CV</label>
                    <input type="file" name="cv" class="form-control-file {{ set_error('cv') }}" id="input-cv">
                    {!! get_error('cv') !!}
                </div>
                <div class="form-group">
                    <button class="btn mt-4 btn--default btn-success shadow-sm">Kirim</button>
                </div>
            </form>
        </div>
    </div>
@endsection
