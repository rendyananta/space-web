@extends('layouts.default')

@section('sub-body')
    <h2 class="display mt-5">Gawe Event</h2>
    <h5 class="mt-4">Cek ketersediaan jadwal penggunaan ruangan <a href="">di sini</a></h5>
    <div class="row mt-5">
        <div class="col-md-6">
            @include('layouts.components.errors')

            <form action="{{ route('event.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method("post")
                <div class="form-group">
                    <label for="input-name" class="col-form-label">Nama Event</label>
                    <input type="text" name="name" class="form-control {{ set_error('name') }}" id="input-name" value="{{ old('name') }}">
                    {!! get_error('name') !!}
                </div>
                <div class="form-group">
                    <label for="input-start_at" class="col-form-label">Waktu mulai</label>
                    <input type="datetime-local" name="start_at" class="form-control {{ set_error('start_at') }}" id="input-start_at" value="{{ old('start_at') }}">
                    {!! get_error('start_at') !!}
                </div>
                <div class="form-group">
                    <label for="input-end_at" class="col-form-label">Waktu berakhir</label>
                    <input type="datetime-local" name="end_at" class="form-control {{ set_error('end_at') }}" id="input-end_at" value="{{ old('end_at') }}">
                    {!! get_error('end_at') !!}
                </div>
                <div class="form-group">
                    <label for="input-purpose" class="col-form-label">Tujuan</label>
                    <textarea name="purpose" class="form-control {{ set_error('purpose') }}" id="input-purpose" cols="15" rows="5">{!! old('purpose') !!}</textarea>
                    {!! get_error('purpose') !!}
                </div>
                <div class="form-group">
                    <label for="input-audience_target" class="col-form-label">Target Pendengar</label>
                    <input type="number" name="audience_target" class="form-control {{ set_error('audience_target') }}" id="input-audience_target" value="{{ old('audience_target') }}">
                    {!! get_error('audience_target') !!}
                </div>
                <div class="form-group">
                    <label for="input-proposal" class="col-form-label">Unggah Proposal</label>
                    <input type="file" name="proposal" class="form-control-file {{ set_error('proposal') }}" id="input-proposal">
                    {!! get_error('proposal') !!}
                </div>
                <div class="form-group">
                    <button class="btn mt-4 btn--default btn-success shadow-sm">Kirim</button>
                </div>
            </form>
        </div>
    </div>
@endsection
