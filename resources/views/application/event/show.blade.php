<div class="col-md-6">

    <div class="row">
        <div class="col-sm-6">
            Nama Event
        </div>
        <div class="col-sm-6">
            <strong>{{ $application->applicable->name }}</strong>
        </div>

        <div class="col-sm-6">
            Tujuan Event
        </div>
        <div class="col-sm-6">
            <strong>{{ $application->applicable->purpose }}</strong>
        </div>


        <div class="col-sm-6">
            Penyelenggaraan Event
        </div>
        <div class="col-sm-6">
            <strong>{{ $application->applicable->start_at->format('D M Y - H:i') }}
                s.d<br> {{ $application->applicable->end_at->format('D M Y - H:i') }}</strong>
        </div>

        <div class="col-sm-6">
            Target Audiens
        </div>
        <div class="col-sm-6">
            <strong>{{ $application->audience_target }}</strong>
        </div>

        <div class="col-sm-6">
            Proposal
        </div>
        <div class="col-sm-6">
            <strong><a href="{{ asset(Storage::url($application->applicable->proposal)) }}">Lihat Proposal</a></strong>
        </div>

        <div class="col-sm-6">
            Status
        </div>
        <div class="col-sm-6">
            <strong>{{ $application->status ?? "-" }}</strong>
        </div>

        <div class="col-sm-6">
            Disetujui Pada
        </div>
        <div class="col-sm-6">
            <strong>
                @if($application->status === null)
                    -
                @else
                    @if($application->status == \App\Entities\Application::STATUS_APPROVED)
                        {{ $application->approved_at->format('d M Y - H:i') }}
                    @else
                        {{ $application->rejected_at->format('d M Y - H:i') }}
                    @endif
                @endif
            </strong>
        </div>

    </div>

</div>
