@extends('layouts.default')

@section('sub-body')
    <h2 class="display mt-5">Jadwal Penggunaan Ruang</h2>

    <div class="row mt-5">
        <div class="col-md-6">
            <form class="form-inline">
                <div class="form-group mb-2">
                    <label for="input-date" class="col-form-label">Tanggal</label>
                    <input type="date" class="ml-2 form-control" id="input-date" value="email@example.com">
                </div>
                <button type="submit" class="btn ml-4 btn-primary mb-2">Cek</button>
            </form>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col">
            <section class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">Waktu</th>
                        <th scope="col">Peminjam</th>
                        <th scope="col">Nama Event</th>
                    </tr>
                    </thead>
                    <tbody>
                        @each('application.event.item_event', $events, 'event', 'application.event.item_empty')
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
