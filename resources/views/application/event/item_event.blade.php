<tr>
    <td>{{ $event->start_at->format('D M Y - H:i') }} s.d {{ $event->end_at->format('D M Y - H:i') }}</td>
    <td>{{ $event->application->applicant->name }}</td>
    <td>{{ $event->name }}</td>
</tr>
