@extends('layouts.default')

@section('sub-body')
    <h2 class="display mt-5">Ubah Profil</h2>

    <div class="row mt-5">
        <div class="col-md-6">
            @include('layouts.components.errors')

            <form action="{{ route('profile.update') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method("put")

                <div class="form-group row">
                    <div class="col-sm-3">
                        <img src="{{ auth()->user()->avatar ? asset(Storage::url(auth()->user()->avatar)) : url('/img/default-avatar.png') }}" class="img-fluid" style="width: 80px" alt="">
                    </div>
                    <div class="col">
                        <label for="input-avatar">Pilih file untuk ubah avatar</label>
                        <input type="file" name="avatar" class="form-control-file" id="input-avatar">
                    </div>
                </div>

                <div class="form-group">
                    <label for="input-name" class="col-form-label">Nama</label>
                    <input type="text" name="name" class="form-control {{ set_error('name') }}" id="input-name" value="{{ old_or_value('name', true, auth()->user()) }}">
                    {!! get_error('name') !!}
                </div>
                <div class="form-group">
                    <label for="input-email" class="col-form-label">Surel / email</label>
                    <input type="text" name="email" class="form-control {{ set_error('email') }}" id="input-email" value="{{ old_or_value('email', true, auth()->user()) }}">
                    {!! get_error('email') !!}
                </div>
                <div class="form-group">
                    <label for="input-dob" class="col-form-label">Tanggal Lahir</label>
                    <input type="date" name="dob" class="form-control {{ set_error('dob') }}" id="input-dob" value="{{ auth()->user()->dob->format('Y-m-d') }}">
                    {!! get_error('dob') !!}
                </div>
                <div class="form-group">
                    <label for="input-gender" class="col-form-label">Jenis Kelamin</label>
                    <select name="gender" id="input-gender" class="form-control {{ set_error('gender') }}">
                        <option value="">Pilih Jenis Kelamin</option>
                        @foreach(\App\Entities\User::GENDERS as $key => $gender)
                            <option value="{{ $key }}" {{ set_selected(old_or_value('gender', true, auth()->user()) == $key) }}>{{ $gender }}</option>
                        @endforeach
                    </select>
                    {!! get_error('gender') !!}
                </div>
                <div class="form-group">
                    <label for="input-occupation" class="col-form-label">Pekerjaan</label>
                    <select name="occupation" id="input-occupation" class="form-control {{ set_error('occupation') }}">
                        <option value="">Pilih Pekerjaan</option>
                        @foreach(\App\Entities\User::OCCUPATIONS as $occupation)
                            <option value="{{ $occupation }}" {{ set_selected(old_or_value('occupation', true, auth()->user()) == $occupation) }}>{{ $occupation }}</option>
                        @endforeach
                    </select>
                    {!! get_error('occupation') !!}
                </div>
                <div class="form-group">
                    <label for="input-institution" class="col-form-label">Institusi</label>
                    <input type="text" name="institution" class="form-control {{ set_error('institution') }}" id="input-institution" value="{{ old_or_value('institution', true, auth()->user()) }}">
                    {!! get_error('institution') !!}
                </div>
                <div class="form-group">
                    <button class="btn mt-4 btn--hero btn-success shadow-sm" type="submit">Ubah</button>
                </div>
            </form>
        </div>
    </div>
@endsection
