@extends('layouts.default')

@section('sub-body')
    <h2 class="display mt-5">Profil</h2>

    <div class="row mt-5">
        <div class="col-md-6">

            <div class="row">
                <div class="col-sm-3">
                    <img src="{{ auth()->user()->avatar ? asset(Storage::url(auth()->user()->avatar)) : url('/img/default-avatar.png') }}" class="img-fluid" style="width: 380px" alt="">
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-sm-6">
                    Nama
                </div>
                <div class="col-sm-6">
                    <strong>{{ auth()->user()->name }}</strong>
                </div>
                <div class="col-sm-6">
                    Surel / e-mail
                </div>
                <div class="col-sm-6">
                    <strong>{{ auth()->user()->email }}</strong>
                </div>

                <div class="col-sm-6">
                    Tanggal Lahir
                </div>
                <div class="col-sm-6">
                    <strong>{{ auth()->user()->dob->format('d F Y') }}</strong>
                </div>

                <div class="col-sm-6">
                    Jenis Kelamin
                </div>
                <div class="col-sm-6">
                    <strong>{{ \App\Entities\User::GENDERS[auth()->user()->gender] }}</strong>
                </div>

                <div class="col-sm-6">
                    Pekerjaan
                </div>
                <div class="col-sm-6">
                    <strong>{{ auth()->user()->occupation }}</strong>
                </div>

                <div class="col-sm-6">
                    Institusi
                </div>
                <div class="col-sm-6">
                    <strong>{{ auth()->user()->institution }}</strong>
                </div>

                <div class="col-sm-6 mt-5">
                    <a href="{{ route('profile.edit') }}" class="btn btn--default btn-warning shadow-sm">Ubah Profil</a>
                </div>
                <div class="col-sm-6 mt-5">
                    <a href="{{ route('profile.password.reset.form') }}" class="btn btn--default btn-danger shadow-sm">Ubah Sandi</a>
                </div>

            </div>

        </div>
    </div>
@endsection
