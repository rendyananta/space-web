@extends('layouts.default')

@section('sub-body')
    <h2 class="display mt-5">Ubah Sandi</h2>

    <div class="row mt-5">
        <div class="col-md-6">
            @include('layouts.components.errors')

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form action="{{ route('profile.password.reset') }}" method="post">
                @csrf
                @method("post")

                <div class="form-group">
                    <label for="input-old-password" class="col-form-label">Sandi Lama</label>
                    <input type="password" name="old_password" class="form-control {{ set_error('old_password') }}" id="input-old-password">
                    {!! get_error('old_password') !!}
                </div>

                <div class="form-group">
                    <label for="input-password" class="col-form-label">Sandi Baru</label>
                    <input type="password" name="password" class="form-control {{ set_error('password') }}" id="input-password">
                    {!! get_error('password') !!}
                </div>
                <div class="form-group">
                    <label for="input-password-confirmation" class="col-form-label">Konfirmasi Sandri Baru</label>
                    <input type="password" name="password_confirmation" class="form-control {{ set_error('password_confirmation') }}" id="input-password-confirmation">
                    {!! get_error('password_confirmation') !!}
                </div>
                <div class="form-group">
                    <button class="btn mt-4 btn--hero btn-danger shadow-sm">Ubah Sandi</button>
                </div>
            </form>
        </div>
    </div>
@endsection
