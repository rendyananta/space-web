<footer class="py-5">
    <div class="container">
        <hr>
        <h2 class="display mt-5">Tentang</h2>
        <div class="row">
            <div class="col-md-6 mt-5">
                <p>Sebagai bagian dari visi Wali Kota Surabaya, Tri Rismaharini, untuk menjadikan Surabaya sebagai sentra kreatif dan teknologi di tingkat global, KORIDOR merupakan langkah awal untuk membangun dan memperkuat pondasi ekonomi kreatif di tingkat lokal.</p>
                <p>KORIDOR adalah komitmen Pemerintah Kota Surabaya untuk menciptakan ekosistem yang memberdayakan para kreator, inovator, dan entrepreneur lokal untuk menciptakan inovasi dan mampu bersaing di tingkat global.</p>

                <div class="mt-5">
                    <a href="/faq">Pertanyaan yang Sering Diajukan ></a> <br>
                    <a href="{{ route('event.index') }}">Cek Ketersediaan Ruangan ></a>
                </div>
            </div>
            <div class="col-md-5 offset-md-1">
                <img src="{{ asset('img/logo/Space.png') }}" class="img-fluid" style="width: 300px" alt="">
            </div>
        </div>
    </div>
</footer>
