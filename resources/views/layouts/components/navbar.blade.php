<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
    <a class="navbar-brand" href="{{ url('/') }}" style="font-weight: 500">
        SPACE
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="container">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item {{ Request::is("/") ? "active" : "" }}">
                    <a class="nav-link" href="/">Beranda <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item pl-5">
                    <a class="nav-link" href="/#facilities">Fasilitas</a>
                </li>
                <li class="nav-item pl-5">
                    <a class="nav-link" href="/#activities">Aktifitas</a>
                </li>
                <li class="nav-item pl-5">
                    <a class="nav-link" href="/#galleries">Galeri</a>
                </li>
                <li class="nav-item pl-5">
                    <a class="nav-link" href="/#location">Lokasi</a>
                </li>
                <li class="nav-item pl-5">
                    <a class="nav-link" href="/#partners">Mitra</a>
                </li>
                <li class="nav-item pl-5">
                    <a class="nav-link" href="{{ route('event.index') }}">Cek Ruang</a>
                </li>
            </ul>
            <ul class="navbar-nav">
                @if(auth()->check())
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{{ auth()->user()->name }}</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('profile.index') }}">Profil</a>
                            <a class="dropdown-item" href="{{ route('application.index') }}">Daftar Aplikasi</a>
                            <div role="separator" class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('auth.logout') }}">Keluar</a>
                        </div>
                    </li>
                @else
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('auth.login.form') }}">Masuk</a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
