@if(! is_null(session('errors')))
    @if(count(array_filter(array_keys(session('errors')->getBag('default')->toArray()))) < 1)
        <div class="alert alert-danger">
            {{ session('errors')->first() }}
        </div>
    @endif
@endif
