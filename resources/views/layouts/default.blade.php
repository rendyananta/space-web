@extends('layouts.skeleton')

@section('body')
    @include('layouts.components.navbar')

    <div class="container pt-5">
        @yield('sub-body')
    </div>

    @include('layouts.components.footer')
@endsection
