<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkingSpacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('working_spaces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('address');
            $table->string('phone');
            $table->text('cv');
            $table->text('portfolio');
            $table->text('proposal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('working_spaces');
    }
}
