<?php

function set_active($pattern)
{
    return request()->routeIs($pattern) ? 'active' : '';
}

function set_selected($condition)
{
    return $condition ? 'selected' : '';
}

function set_disabled($cond)
{
    return $cond ? 'disabled' : '';
}

function old_or_value($attribute, $edit = false, $value = null, $assoc = false)
{
    if ($edit) {
        return $assoc ? Arr::get($value, $attribute) : $value->{$attribute};
    }

    return old($attribute);
}

if (! function_exists('get_error')) {
    function get_error($input)
    {
        $errors = session('errors');

        return isset($errors) ? '<div class="invalid-feedback">'.$errors->first($input).'</div>' : '';
    }
}

if (! function_exists('set_error')) {
    function set_error($input)
    {
        $errors = session('errors');
        if (isset($errors)) {
            return $errors->has($input) ? 'is-invalid' : '';
        }

        return '';
    }
}

// Number helpers

/**
 * @param $n
 * @return string
 * Use to convert large positive numbers in to short form like 1K+, 100K+, 199K+, 1M+, 10M+, 1B+ etc
 */
function number_format_short($n)
{
    if ($n >= 0 && $n < 1000) {
        // 1 - 999
        $n_format = floor($n);
        $suffix = '';
    } elseif ($n >= 1000 && $n < 1000000) {
        // 1k-999k
        $n_format = floor($n / 1000);
        $suffix = 'K+';
    } elseif ($n >= 1000000 && $n < 1000000000) {
        // 1m-999m
        $n_format = floor($n / 1000000);
        $suffix = 'JT+';
    } elseif ($n >= 1000000000 && $n < 1000000000000) {
        // 1b-999b
        $n_format = floor($n / 1000000000);
        $suffix = 'M+';
    } elseif ($n >= 1000000000000) {
        // 1t+
        $n_format = floor($n / 1000000000000);
        $suffix = 'T+';
    } elseif ($n >= 1000000000000000) {
        $n_format = floor($n / 1000000000000000);
        $suffix = 'KT+';
    } else {
        $n_format = floor($n / 1000000000000000);
        $suffix = 'KT+';
    }

    return ! empty($n_format.$suffix) ? $n_format.$suffix : 0;
}


function thousand($value)
{
    return number_format($value, 0, ',', '.');
}

function rupiah($value, $abbreviate = false)
{
    $formatted = $abbreviate ? number_format_short($value) : thousand($value);

    return 'Rp '.$formatted;
}
