<?php

namespace App\Entities;

use App\Notifications\Account\ResetPassword;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'dob', 'gender',
        'occupation', 'institution', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @const OCCUPATIONS
     */
    const OCCUPATIONS = [
        'Pelajar',
        'Mahasiswa',
        'Freelancer',
        'Lainnya'
    ];

    /**
     * @const GENDERS
     */
    const GENDERS = [
        'm' => 'Laki-laki',
        'f' => 'Perempuan',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * Define hasMany relation with Application Model
     *
     * @return HasMany
     */
    public function applications()
    {
        return $this->hasMany(Application::class, 'user_id', 'id');
    }

    /**
     * @return array|mixed
     */
    public function getDobAttribute()
    {
        return Carbon::createFromFormat('Y-m-d', $this->getOriginal('dob'));
    }
}
