<?php


namespace App\Entities\contracts;


interface Applicable
{

    /**
     * Get Applicable type string
     *
     * @return string
     */
    function getApplicationType(): string;

    /**
     * Get Applicable type code string
     *
     * @return string
     */
    function getApplicationTypeCode(): string;

}
